# shell_util

This project contains a number of useful shell utilities that can be used in other scripts by sourcing them.

## Usage

All utility scripts can be sourced into your scripts, the scripts itself have dependencies on each other and they are protected agains multiple inclusion. 
The only important note here is that the scripts need the UTIL_DIR variable to be set, since the scripts can be called from any place. 

```sh
UTIL_DIR="${UTIL_DIR:- ../shell_util}"
# shellcheck source=/dev/null
. "${UTIL_DIR}/log.sh"
# shellcheck source=/dev/null
. "${UTIL_DIR}/git.sh"
....

```