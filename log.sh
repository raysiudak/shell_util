#!/bin/sh
# A utility library script that can be sourced and used in other scripts
# This script contains a collection of useful log operations
#
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: AGPL-3.0+

if [ -n "${__UTIL_LOG__}" ]
then
  return 0
else
  readonly __UTIL_LOG__=1
fi

log_info()
{
    echo "$(tput bold) ${*} $(tput sgr 0)"
}

# Print orange text, used for warnings.
log_warning()
{
    echo "$(tput bold)$(tput setaf 172) ${*} $(tput sgr 0)"
}

# Print red text, used for errors.
log_error()
{
    echo "$(tput bold)$(tput setaf 1) ${*} $(tput sgr 0)"
}

#info "This info message is shown in bold."
#warning "This warning message is shown in orange."
#error "This warning message is shown in red."