#!/bin/sh
# A utility library script that can be sourced and used in other scripts
# This script contains a collection of useful git operations
#
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# SPDX-License-Identifier: AGPL-3.0+

if [ -n "${__UTIL_GIT__}" ]
then
  echo "already included"
  return 0
else
  readonly __UTIL_GIT__=1
fi

if ! command -v git;then
    error "Error: git not found" && return 1
fi

# shellcheck source=/dev/null
. "${UTIL_DIR}/log.sh"


# Make sure a git checkout is clean,
# make a fresh clone if there is none,
# else run all the git foo to clean it.
git_clean()
{
    path="${1}"
    repo="${2}"
    branch="${3}"

    cwd="$(pwd)"

    if [ "${branch}" = "" ]; then
        branch="master"
    fi

    log_info "Making sure ${path} is clean and fresh from ${repo} with branch ${branch}."

    if [ ! -e "${path}" ]; then
        git clone --recursive -b "${branch}" "${repo}" "${path}"
    fi

    cd "${path}" || return 1
    git clean -f -x -d
    git reset --hard
    git checkout "${branch}"
    git pull --recurse-submodules
    cd "${cwd}" || return 1
}